$(document).ready(()=>{
	$.when( $.post("./shell.php/?uname") ) .then((data, status_text, req) => {
		if(req.status == 200) {
			res = data.split('\n');
			res = res.join("").replace("\\", "").replace("<", "");
			res = JSON.parse(res);

			let terminal = new Terminal(document.body, res.host, res.user, res.pwd, 880, 500, "black", "white");
			terminal.draw_terminal();

			$("body").on("keyup", (event)=>{
				terminal.enter_cmd(event);
			});
		}
	});
});