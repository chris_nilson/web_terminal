function shell(cli) {
  let cmd = cli.line.get_cmd().trim();
  let url = "./shell.php/?cmd="+cmd+"&pwd="+cli.pwd;

  $.when( $.post(url) ) .then((data, status_text, req)=> {
    if(req.status == 200) {

      switch(cmd.split(" ")[0]){
        case "":
            cli.line = new Line(cli.box, cli.host, cli.user, cli.pwd);
            break;

        case "exit":
            cli.box.remove();
            break;

        case "clear":
            cli.box.innerHTML = "";
            cli.line = new Line(cli.box, cli.host, cli.user, cli.pwd);
            break;

        case "cd":
          if(cmd.split(" ").length > 2) {
            cli.message("sh: 1: too many argument");
          }

          if(data == "") {
            url = "./shell.php/?cmd=pwd&pwd="+cli.pwd +"; cd "+ cmd.split(" ")[1];
            $.when( $.post(url) ) .then((data, status_text, req)=> {
              if(req.status == 200) {
                cli.pwd = data.trim() == "" ? cli.home : data.trim();
                cli.line = new Line(cli.box, cli.host, cli.user, cli.pwd);
              }
            });
          }
          break

        default:
          $.when( $.post(url) ) .then((data, status_text, req)=> {
            if(req.status == 200) {
              cli.message(data);
              cli.line = new Line(cli.box, cli.host, cli.user, cli.pwd);
            }
          });
          break;
      }

    }
  });
}