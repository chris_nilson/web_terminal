let $home = null;
let req = new XMLHttpRequest();
req.open("post", "./shell.php/?uname");
req.send();
req.onload = function() {
  let res = req.responseText.split('\n');
  res = res.join("").replace("\\", "").replace("<", "");
  res = JSON.parse(res);
  $home = res.pwd;
}

class Line {
    constructor(root, host, user, pwd) {

      	let used_inputs = document.querySelectorAll("input");
      	for(let i=0; i<used_inputs.length; i++) {
      		let input = used_inputs[i];
      		input.setAttribute("disabled", "");
      	}

        this.root = root;
        this.line = document.createElement("p");
        this.pwd = document.createElement("strong");
        this.host_user = document.createElement("strong");
        this.cmd_block = document.createElement("input");

        this.line.style.width = this.root.style.width;

    		this.cmd_block.style.backgroundColor = "transparent";
    		this.cmd_block.style.color = "blue";
    		this.cmd_block.style.border = "none";
    		this.cmd_block.style.padding = "0px 4px";

        this.cmd_block.style.display = "inline";
        this.line.style.margin = "0px";

        pwd = pwd.replace($home, "~");

        this.host_user.innerText = user + "@" + host + " :";
        this.pwd.innerHTML = pwd + "<span style='color: wheat'>$</span> ";
        this.line.appendChild(this.host_user);
        this.host_user.appendChild(this.pwd);
        this.line.appendChild(this.cmd_block);
        this.root.appendChild(this.line);

        this.line.style.display = "flex";

        this.cmd_block.style.flex = "2";

        this.color = {
            host_user: "greenyellow",
            cmd: "crimson",
            pwd: "orange"
        };
        this.host_user.style.color = this.color.host_user;
        this.pwd.style.color = this.color.pwd;
        this.cmd_block.style.color = this.color.cmd;
    		this.cmd_block.style.fontWeight = "bold";

    		this.cmd_block.focus();
    		this.cmd_block.style.width = "250px";

    }

    edit(cmd) {
        this.cmd_block.value = cmd;
    }
    get_cmd() {
        return this.cmd_block.value;
    }
    get_line() {
        return this.line;
    }
}
