let term = null;
class Terminal {
    constructor(root, host, user, pwd, width = 700, height = 400, bg_color = "black", fg_color = "white") {
        this.box = document.createElement("div");
        this.box.style.backgroundColor = bg_color;
        this.box.style.color = fg_color;
        this.box.style.width = width + "px";
        this.box.style.height = height + "px";
        this.box.style.overflowX = "hidden";
        this.box.style.overflowy = "auto";
        this.box.style.padding = "4px";
        this.box.style.fontSize = "14px";
        this.box.style.borderRadius = "2px";
        this.history = new Array(10);
        this.current_history = 0;
        this.host = host;
        this.user = user;
        this.pwd = pwd;
        this.home = pwd;
        this.message(`Coded and designed by Christian CHABI
        in browser terminal simulator version 1.0.7
        email: ethikoslabs@gmail.com
        `, "warning");
        this.line = new Line(this.box, this.host, this.user, this.pwd);
        root.appendChild(this.box);
        this.box.addEventListener("mouseover", this.focus_on);
        this.box.addEventListener("click", this.focus_on);
    }

    draw_terminal(color = "silver") {
        this.box.style.border = "2px solid "+color;
    }

    enter_cmd(event) {
        let except_keys = new Array(9, 13, 16, 17, 18, 20, 27, 33, 34, 35, 36, 38, 40, 45, 46, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122, 123, 225, 91);
        if (in_array(event.which, except_keys)) {
	          event.preventDefault();
            if (event.which == 13) {
                if(this.history < 10) {
                    if(!in_array(this.line.get_cmd, [undefined, null, ""]))
                        this.history.push(this.line.get_cmd());
                }
                else {
                    if(!in_array(this.line.get_cmd, [undefined, null, ""]))
                        this.history.shift();
                        this.history.push(this.line.get_cmd());
                }
                shell(this);
                this.current_history = 0;

            }
            else if(event.which == 38) {
                let hist_cmd = this.history[this.history.length - ++this.current_history];
                if(!in_array(hist_cmd, ["", null, undefined])) {
                    this.line.edit(hist_cmd);
                }
            }

            else if(event.which == 40) {
                let hist_cmd = this.history[this.history.length - --this.current_history];
                if(!in_array(hist_cmd, ["", null, undefined])) {
                    this.line.edit(hist_cmd);
                }
            }
        }
    }

  	focus_on() {
  		let inputs = this.querySelectorAll("input");
  		inputs[inputs.length-1].focus();
  	};

    message(text, error_type) {
        let span = document.createElement("span");
        span.innerText = text;

        let errors = {
            "warning": "orange",
            "fatal" : "red",
            "syntax": "yellow"
        };

        if(in_array(error_type, errors.key)){
            console.log(error_type);
        }

        switch(error_type) {
            case "warning":
                span.style.color = errors.warning;
                break;
            case "fatal":
                span.style.color = errors.fatal;
                break;
            case "syntax":
                span.style.color = errors.syntax;
                break;
            default:
                null;
        }
        this.box.appendChild(span);
    }
}
