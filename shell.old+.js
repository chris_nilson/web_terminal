function shell(cli) {
    let cmd = cli.line.get_cmd().trim();
    let url = "./shell.php/?cmd="+cmd+"&pwd="+cli.pwd;

    let req = new XMLHttpRequest();
    req.open("post", url);

    switch(cmd.split(" ")[0]){
        case "":
	          cli.line = new Line(cli.box, cli.host, cli.user, cli.pwd);
            break;

        case "exit":
            cli.box.remove();
	          break;

        case "clear":
            cli.box.innerHTML = "";
	          cli.line = new Line(cli.box, cli.host, cli.user, cli.pwd);
            break;

        case "cd":

              if(cmd.split(" ").length > 2) {
                cli.message("sh: 1: too many argument");
              }

              if(req.responseText == "") {
                url = "./shell.php/?cmd=pwd&pwd="+cli.pwd +"; cd "+ cmd.split(" ")[1];
                // url = "./shell.php/?cmd=&pwd="+cli.pwd;
                // url = "./shell.php/?pwd="+cli.pwd;
                req.open("post", url);
                req.send();

                req.onload = function() {
                  cli.pwd = req.responseText.trim();
                  cli.line = new Line(cli.box, cli.host, cli.user, cli.pwd);
                }
              }
              else
                cli.line = new Line(cli.box, cli.host, cli.user, cli.pwd);
              console.log(url);
            break

        default:
          req.send();
          req.onload = function() {
            cli.message(req.responseText);
            cli.line = new Line(cli.box, cli.host, cli.user, cli.pwd);
          }
    	    break;
        }
}
